/*
    Copyright (C) 2013-2014  Moncef Mechri

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <cstdio>
#include <cstdlib>
#include <ctime> //clock_gettime
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "benchmark_util.h"

#ifdef __linux__
#include <sys/mman.h>
#endif

using namespace std;


int main(int argc, char** argv)
{   
    struct command_line_options opt = parse_command_line(argc, argv);
    
    cout << "Power of 2: " << opt.power_of_2 << endl;
    
    struct runtime_settings settings = configure_settings(&opt);

    cout << "Working set size: " << settings.working_set_size << endl;

    if (settings.working_set_size < sizeof(struct elem))
    {
        cerr << "The working set is smaller than the size of one element. Exiting..." << endl;
        exit(1);
    }

    cout << "size of struct elem: " << sizeof(struct elem) << endl;
    
    cout << "Nb elements: " << settings.nb_elements << endl;
    
    struct elem* array;
    
    const int array_size = settings.nb_elements * sizeof(struct elem);

    /*For now, large page support uses THP (Transparent Huge Pages, added in 2.6.38).
     *A memory allocation aligned on the large page size is supposed to allocate a large page.
     *If /sys/kernel/mm/transparent_hugepage/enabled is set to [always], then it is enough to do an aligned allocation
     *to use large pages
     *If it is set to [madvise], large pages will only be allocated if madvise() is used.
     *If it is set to [never], or if THP is not available at all, then large pages won't be used and the program
     *will use regular pages*/
    if (opt.large_pages)
    {
        /*Linux doesn't always allocate large pages when the alloc size is smaller than the size of a large pages 
         * (or one of its multiples).
         * To ensure that it will use large pages, we allocate using a size which is a multiple of the page size.
         * It might look like a waste of space but this space would be wasted anyway when a large page is allocated*/
        const int size_aligned_on_large_page_size = (array_size + HUGEPAGE_SIZE - 1) & ~(HUGEPAGE_SIZE - 1);

        if (posix_memalign((void**) &array, HUGEPAGE_SIZE, size_aligned_on_large_page_size))
        {
            perror("posix_memalign failed\n");
            exit(1);
        }

        madvise(array, size_aligned_on_large_page_size, MADV_HUGEPAGE);
        cout << "Using large pages" << endl;
    }

    else
    {
        if (posix_memalign((void**) &array, CACHE_LINE_SIZE, array_size))
        {
            perror("posix_memalign failed\n");
            exit(1);
        }
    }

    switch (opt.mode)
    {
        case command_line_options::SEQUENTIAL_READ:
            link_sequentially(array, settings.nb_elements);
            sequential_walk(array, &settings);
            break;
        case command_line_options::RANDOM_READ:
            link_randomly(array, settings.nb_elements);
            random_walk(array, &settings);
            break;
        default:
            cerr << "Wrong execution mode" << endl;
            exit(1);
    }

    cout << endl << endl;
    
    free(array);
    return 0;
}
