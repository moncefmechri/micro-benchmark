#!/usr/bin/sh

RESULTS_PATH="./results/"
FILES_LIST=$(ls $RESULTS_PATH/*.raw)

for i in $FILES_LIST
do
    #this is used to set the plot title according to the datafile name. For example is the datafile is named results_sequential_read_NPAD_7.raw , 
    #MODE will contain [sequential, read, 7]
    MODE=($(basename "$i" .raw | cut -d _ -f 2,3,5 --output-delimiter=" "))
    gnuplot -e "set terminal pngcairo enhanced size 1000,800; set bmargin 8; set rmargin 5; set grid; set output \"$RESULTS_PATH/$(basename $i .raw).png\"; set title \"${MODE[0]} ${MODE[1]}, NPAD=${MODE[2]}\"; \
                set xlabel \"Working set size (Bytes)\" offset 0,-4; set xtics rotate by 60 offset 0,-4; set ylabel \"Time per element (nanoseconds)\"; \
                plot \"$i\" using 2:xtic(1) with linespoint notitle;" 
done
