set terminal png enhanced
set output "./results/out.png"
set title "Sequential read"
set xlabel "Working set size (Bytes)"
set ylabel "Time per element (nanoseconds)"
set logscale x 2
set format x '2^{%L}'
plot "./results/results_sequential_read_NPAD_7.raw" using (2**$1):2 with linespoint 

