/*
    Copyright (C) 2013-2014  Moncef Mechri

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <iostream>
#include <sstream>
#include <cstdlib> //srand
#include <cstring> //strcmp
#include "benchmark_util.h"
#include <algorithm>

#include <time.h>

#include "tclap/CmdLine.h"

using namespace std;
using namespace TCLAP;

void expand_filename(string& filename)
{
    stringstream ss;
    ss << NPAD;
    
    filename.append("_NPAD_");
    filename.append(ss.str());
    filename.append(".raw");
}

struct command_line_options parse_command_line(int argc, char** argv)
{
    struct command_line_options opt;
    
    CmdLine cmd("Micro benchmark: A cache and memory benchmark");

    SwitchArg arg_seq_read("", "sequential-read", "Traverses a sequentially linked working set");
    SwitchArg arg_random_read("", "random-read", "Traverses a randomly linked working set");
    cmd.xorAdd(arg_seq_read, arg_random_read);

    UnlabeledValueArg<int> arg_power_of_2("exponent", "An integer that will be used as the power of 2 \
                                                       to determine the working set size", true, 0, "exponent", cmd);

    SwitchArg arg_large_pages("", "large-pages", "Allocates the working set on large pages (Only available on Linux with THP support enabled for now)", cmd);

    cmd.parse(argc, argv);

    if (arg_seq_read.isSet())
        opt.mode = command_line_options::SEQUENTIAL_READ;

    else if (arg_random_read.isSet())
        opt.mode = command_line_options::RANDOM_READ;

    opt.power_of_2 = arg_power_of_2.getValue();

    if (opt.power_of_2 < 0 || opt.power_of_2 > 30)
    {
        cerr << "Not a valid power of 2" << endl;
        exit(1);
    }
    
    if (arg_large_pages.isSet())
    {
#ifdef __linux__
        opt.large_pages = true;
#else
        cerr << "Large pages support is only available on Linux for now" << endl;
        exit(1);
#endif
    }

    else
        opt.large_pages = false;
    
    return opt;
}

struct runtime_settings configure_settings(const struct command_line_options* opt)
{
    struct runtime_settings settings;

    settings.options = (struct command_line_options*) opt;

    const size_t tmp_working_set_size = 1 << opt->power_of_2;//Just used in the next line 
    //settings.working_set_size = opt->intermediate_value ? tmp_working_set_size + tmp_working_set_size / 2 : tmp_working_set_size; 
    settings.working_set_size = tmp_working_set_size;

    settings.nb_elements = settings.working_set_size / sizeof(struct elem);

    return settings;
}

//Link sequentially and circularly all the elements in array 
void link_sequentially(struct elem* array, const size_t nb_elements)
{
    struct elem *current = array;
    
    if (nb_elements < 1)
        return;
    
    for (size_t i = 0; i < nb_elements - 1; ++i)
    {
        current->next = array + i;
        current = current->next;
    }
    
    if (nb_elements > 1)
    {
        current->next = array + (nb_elements - 1);
        
        //This makes the list circularly linked
        current->next->next = array;
    }
    
    else //the array contains exactly one element, and we link it to itself
        current->next = current;

}

void link_randomly(struct elem* array, const size_t nb_elements)
{
    int seed = time(NULL);
    srand(seed);
    
    size_t* random_indexes = (size_t*) malloc(nb_elements * sizeof(size_t));
    if (!random_indexes)
    {
        cerr << "malloc failed in link_randomly()" << endl;
        exit(1);
    }
    
    for (size_t i = 0; i < nb_elements; ++i)
    {
        random_indexes[i] = i;
    }
    
    //Properly shuffle the array. See http://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
    for (size_t i = 0; i < nb_elements -1; ++i)
    {
        swap(random_indexes[i], random_indexes[rand() % (nb_elements - i - 1) + i + 1]);
    }
    for (size_t i = 0; i < nb_elements; ++i)
    {
        array[i].next = array + random_indexes[i];
    }
    
    free(random_indexes);
}

double list_walk(const struct elem* array, const size_t nb_elements)
{
    cout << "Will iterate " << ITER << " times over " << nb_elements << " elements" << endl;
    
    const struct elem* current = array;
    
    //Walk through the list once before benchmarking to reduce the effect of cold misses
    for (unsigned long int i = 0; i < nb_elements; ++i)
        current = current->next;
    
    current = array;
    double initial_clock = get_time();

    for (size_t i = 0; i < ITER; ++i)
    {
        for (size_t j = 0; j < nb_elements ;++j)
        {
            current = current->next;
            //We use this asm statement to make sure that the loop won't be optimized away
            asm volatile ("" :: "r"(current));
        }
    }
    return (get_time() - initial_clock);
}

void sequential_walk(const struct elem* array, const runtime_settings* settings)
{

    string filename = "./results/results_sequential_read";

    expand_filename(filename);
    
    const double total_time = list_walk(array, settings->nb_elements);
    const double time_per_element = total_time / ITER / settings->nb_elements;
    const double time_per_element_in_ns = time_per_element * NANOSEC;
    
    save_results(filename, settings->working_set_size, time_per_element_in_ns);
    
    cout << "==Sequential read==" << endl;
    cout << "total time: " << total_time << endl;
    cout << "time per element (ns): " << time_per_element_in_ns << endl;

}

void random_walk(const struct elem* array, const runtime_settings* settings)
{
    string filename = "./results/results_random_read";
    expand_filename(filename);
    
    const double total_time = list_walk(array, settings->nb_elements);
    const double time_per_element = total_time / ITER / settings->nb_elements;
    const double time_per_element_in_ns = time_per_element * NANOSEC;
    
    save_results(filename, settings->working_set_size, time_per_element_in_ns);
    
    cout << "==Random read==" << endl;
    cout << "total time: " << total_time << endl;
    cout << "time per element (ns): " << time_per_element_in_ns << endl;
}
