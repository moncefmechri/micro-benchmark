/*
    Copyright (C) 2013-2014  Moncef Mechri

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef BENCHMARK_UTIL_H
#define BENCHMARK_UTIL_H

#include <iostream>
#include <fstream>
#include <string>
#include <ctime> //clock_gettime

#define NANOSEC 1000000000
#define ITER 1000
#define CACHE_LINE_SIZE 64
#define MB (1024 * 1024)

#ifdef __linux__
#define HUGEPAGE_SIZE (2 * MB)
#endif

struct elem
{
    struct elem *next;
    long int pad[NPAD];
};

struct command_line_options
{
    enum execution_mode_t
    {
        SEQUENTIAL_READ,
        RANDOM_READ
    };
    
    execution_mode_t mode;
    int power_of_2;
    bool large_pages;
};

struct runtime_settings
{
    command_line_options* options;
    size_t working_set_size;
    size_t nb_elements;
};

struct command_line_options parse_command_line(int argc, char** argv);

struct runtime_settings configure_settings(const struct command_line_options* opt);

//Takes a base filename, and append NPAD and ".raw" to it
void expand_filename(std::string& filename);

//Links sequentially and circularly all the elements in array 
void link_sequentially(struct elem* array, const size_t nb_elements); 

//Links randomly all the elements in array
void link_randomly(struct elem* array, const size_t nb_elements);

//Follows the next link ITER * nb_elements times, and measure the total time
double list_walk(const struct elem* array, const size_t nb_elements);

//Calls list_walk(), save the result in a correctly named file and prints on stdout the output. Assumes that the list is sequentially linked
void sequential_walk(const struct elem* array, const runtime_settings* settings);

//Calls list_walk(), save the result in a correctly named file and prints on stdout the output. Assumes that the list is randomly linked
void random_walk(const struct elem* array, const runtime_settings* settings);

inline double get_time(void)
{
    
    struct timespec tp;
    double ret;
    
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tp);
    
    ret = tp.tv_sec + (double) tp.tv_nsec / NANOSEC;
    
    return ret;
}

template <typename T, typename U>
void gnuplot(std::ostream& os, T first, U second)
{
    os << first << ' ' << second << std::endl;
}

//This function in template in case we decide to use something else than the time (for example, cycles count)
template <typename T>
void save_results(const std::string& filename, const size_t working_set_size, const T result)
{
    std::ofstream file(filename.c_str(), std::ios::app);
    
    if (file.fail())
    {
        perror("File opening failed\n");
        return;
    }
    
    
    gnuplot(file, working_set_size, result);
}


#endif
